{-# LANGUAGE OverloadedStrings #-}
module Controller where

import Model
import Parser

import Data.Text as T


prepareAction :: Arguments -> Either Text ([Contact] -> Text)
prepareAction args = runParser parser args where
    search :: Parser ([Contact] -> Maybe Contact)
    search = parseLit "search" `dropfirst` parseStr `convert` findContact

    chann :: Parser ([Contact] -> [Channel])
    chann = search `convert` (maybe [] channels.)

    select :: Parser ([Channel] -> [Channel])
    select = parseLit "channel" `dropfirst` parseStr `convert` findChannelsWithType


    parser :: Parser ([Contact] -> Text)
    parser = chann `pipe` select `convert` (showAll.)


showAll :: [Channel] -> Text
showAll channs = T.intercalate "\n" $ Prelude.map address channs

