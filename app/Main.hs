{-# LANGUAGE OverloadedStrings #-}
module Main where

import Model
import Controller

import Data.YAML
import Data.Text as T
import Data.Text.IO as T
import System.Environment

import qualified Data.ByteString as B
import qualified Data.ByteString.Lazy as BL

readContacts :: FilePath -> IO (Either String [Contact])
readContacts file = do
    input <- B.readFile file
    let res = decodeStrict input :: Either (Pos, String) [Contact]
    case res of
        Left (errorPos, errorMsg) -> do
            lazyInput <- BL.readFile file
            let location = "error in file '" ++ file ++ "'"
            let message = prettyPosWithSource errorPos lazyInput location ++ errorMsg :: String
            return $ Left message
        Right val -> return $ Right val


main :: IO ()
main = do
    args <- getArgs
    let t_args = Prelude.map T.pack args
    case prepareAction t_args of
        Left error -> T.putStrLn error
        Right action -> do
            c <- readContacts "/home/baptiste/perso/contacts/contacts.yaml"
            case c  of
                Left error -> Prelude.putStrLn error
                Right contacts -> T.putStrLn $ action contacts
