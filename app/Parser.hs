{-# LANGUAGE OverloadedStrings #-}
module Parser where

import Data.Text as T

type Arguments = [Text]
type ParseResult a = Either Text (Arguments, a)
type Parser a = Arguments -> ParseResult a

parseLit :: Text -> Parser Text
parseLit text [] = Left $ T.intercalate "" ["expecting '", text, "'"]
parseLit text (x:xs) = if text == x then Right (xs, text)
                         else Left $ T.intercalate "" ["unexpected token '", x, "'"]

parseStr :: Parser Text
parseStr [] = Left $ "expecting a string"
parseStr (x:xs) = Right (xs, x)

convert :: Parser a -> (a -> b) -> Parser b
convert parser f args = case parser args of
    Left error -> Left error
    Right (finalArgs, value) -> Right (finalArgs, f value)

combine :: (a -> b -> c) -> Parser a -> Parser b -> Parser c
combine f parser1 parser2 args = case parser1 args of
    Left error -> Left error
    Right (newArgs, v1) -> case parser2 newArgs of
        Left error -> Left error
        Right (finalArgs, v2) -> Right (finalArgs, f v1 v2) 

orElse :: Parser a -> Parser a -> Parser a
orElse parser1 parser2 args = case parser1 args of
    Left error -> case parser2 args of
        Left error -> Left error
        Right res -> Right res
    Right res -> Right res

dropfirst = combine (\a -> \b -> b)
pipe = combine $ flip (.)

runParser :: Parser a -> Arguments -> Either Text a
runParser parser args = case parser args of
    Left error -> Left error
    Right ([], value) -> Right value
    Right (remain, _) -> Left $ T.append "unexpected arguments" $ T.unwords remain

-- main = do
--     let parser = (parseLit "test" `dropfirst` parseStr `convert` (\x -> "The string is " ++ x))
--             `orElse` (parseLit "this" `dropfirst` parseStr `convert` (\x -> "I found a " ++ x))
--                  -- parseStr "this" (\x -> "Is this " ++ x ++ " ? Yes, this is " ++ x)
--     case parser ["this", "something"] of
--         Left error -> putStrLn error
--         Right (args, value) -> do
--             putStrLn value
