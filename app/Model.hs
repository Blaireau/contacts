{-# LANGUAGE OverloadedStrings #-}
module Model where

import Data.YAML
import Data.List as L
import Data.Text as T

data Identity = Identity
    { first_name :: Text
    , last_name :: Text
    , birth_data :: Text
    } deriving Show

instance FromYAML Identity where
    parseYAML = withMap "Identity" $ \m -> Identity
        <$> m .: "first-name"
        <*> m .: "last-name"
        <*> m .:? "birth-date" .!= ""

data Contact = Contact
    { identity :: Identity
    , channels :: [Channel]
    } deriving Show

instance FromYAML Contact where
    parseYAML = withMap "Contact" $ \m -> Contact
        <$> m .: "identity"
        <*> m .: "channels"

data Channel = Channel
    { channelType :: Text
    , address :: Text
    , tag :: Text
    } deriving Show

instance FromYAML Channel where
    parseYAML = withMap "Channel" $ \m -> Channel
        <$> m .: "type"
        <*> m .: "address"
        <*> m .:? "tag" .!= ""


nameMatchingScore :: Text -> Contact -> Int
nameMatchingScore name contact
    | l_name == l_last = 10
    | l_name == l_first = 8
    | l_name `T.isPrefixOf` l_last = 6
    | l_name `T.isPrefixOf` l_first = 4
    | otherwise = 0
    where ident = identity contact
          l_name = toLower name
          l_last = toLower $ last_name ident
          l_first = toLower $ first_name ident

findContact :: Text -> [Contact] -> Maybe Contact
findContact name contacts = if score best /= 0 then Just best else Nothing
    where
        best = maximumBy compareScore contacts
        score = nameMatchingScore name
        compareScore a b
            | sa == sb = EQ
            | sa < sb = LT
            | otherwise = GT
            where
                sa = score a
                sb = score b

findChannelsWithType :: Text -> [Channel] -> [Channel]
findChannelsWithType chType = L.filter (\c -> channelType c == chType)
